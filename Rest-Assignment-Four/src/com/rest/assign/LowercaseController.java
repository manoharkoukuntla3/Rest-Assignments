package com.rest.assign;


import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

@Path("/caseConverter")
public class LowercaseController {

	@GET
    @Path("/toLower")  
    public Response toLowerCase(@QueryParam("name")String name  
       ) {  
		System.out.println(name);
        return Response.status(200)  
            .entity(name.toLowerCase())  
            .build();  
    }  
}
